# Boot2Container

<img src="/logo/logo128px_with_padding.png" alt="Logo of the project" align="left" style="margin-right: 20px"/>

**__WARNING__**: The interface is not stable just yet, and using this initramfs
may wipe your drive (not by default though). Use with caution!

Shipping containers have revolutionized the goods industry by standardizing the
way goods are packaged down to the their physical dimensions, load capacity, ...
which made it easy to stack them up on big container ships then offload them to
trains and trucks, and so long and so forth.

Just like physical containers, IT containers have brough this level of
standardization to allow reproducing a work/testing environment anywhere, without
affecting the running host. This explains why containers are now so ubiquitous,
and why they are the basis of the vast majority of automated test systems found
on Github, Gitlab, and other forges!

This projects aims to turn create a generic initramfs that initializes the HW
just enough to download and run containers (docker, OCI). This enables running
to run on your HW the same containers you may already run in your HW-independent
cycles, thus simplifying HW testing!

## Features

 * Small size: Under 20MB, with a goal of achieving sub-10MB
 * Fast boot: Under 10s for the first boot to docker's hello-world, 5s on later boots
 * Simple: no daemons, under 1kLOC of code, easy to generate
 * Maintainable: All the heavy lifting done by Red Hat's [podman](https://podman.io/)

## Options

The initramfs reads its parameters from the kernel command line. The only
required argument there is `b2c.container` which is the address to the container
that needs to be executed. In `grub.cfg`, this could look like this:

    menuentry 'Boot 2 container' --class arch --class gnu-linux --class gnu --class os $menuentry_id_option {
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_gpt
        insmod ext2
        search --set=root --file /vmlinuz
        echo    'Loading Linux ...'
        linux   /vmlinuz b2c.container=docker://hello-world
        echo    'Loading ramdisk ...'
        initrd  /initramfs.linux_amd64.cpio.xz

Here is the list of available options:
 * **Arguments**:
   * **b2c.extra_args_url**: Source additional parameters from an HTTP/HTTPS URL.
     This can be useful to set up confidential volumes without leaking minio
     credentials or fscrypt keys to the containers via the kernel command line.
     Format: Same one as the kernel command line.
     WARNING: `b2c.extra_args_url` will be ignored when specified via an extra
     argument url (no recursion possible).
 * **Containers**:
   * **b2c.container**: Command line of a container to be executed at boot.
     Format: See [podman's image format](https://docs.podman.io/en/latest/markdown/podman-run.1.html#image).
     If you set this parameter multiple times, the containers will be executed one
     after the other, in the same order as they are specified. Examples:
     * `b2c.container=docker://docker.io/library/hello-world`
     * `b2c.container="-ti docker://docker.io/library/alpine:latest /bin/sh"`
   * **b2c.post_container**: Command line of a container to be executed *after* all
     the `b2c.containers`, no matter how the previous container exited. This
     parameter can be repeated multiple times. See `b2c.container` for the command
     line format.
   * **b2c.pipefail**: Stop executing containers as soon as one returns the
   status code 0. This only affects `b2c.container`, not `b2c.post_container`.
 * **Services**:
    * **b2c.ntp_peer**: Set the clock on boot by querying the specified NTP server.
     The value can be an IP address, a fully qualified name, or one of the
     following values:
      * none (default): Do not set the time at boot
      * auto: Use `pool.ntp.org` as a peer
   * **b2c.minio**: Create a minio alias, which can then be referenced in the
     `b2c.volume` specifications.
     Syntax: `b2c.minio="<alias>,<URL>,<ACCESSKEY>,<SECRETKEY>`
 * **Caching**:
   * **b2c.cache_device**: Use a cache to store the image layers, thus saving on network usage:
     * none (default): Do not use any cache, guaranteeing a fresh environment every time
     * auto: Re-use a previously-setup drive, or pick a suitable one, and partition it! (**WILL ERASE YOUR DATA**)
     * reset: Re-format the previously-setup drive, or pick up a suitable one and partition it! (**WILL ERASE YOUR DATA**)
     * /dev/XXX: A path to a block device, or a partition. If the path is to a block device and it does not have a partition labeled `B2C_CACHE`, it will recreate the partition table and format the partition. If the path is to a partition, it will use it directly if it is labeled `B2C_CACHE`, otherwise it will reformat it. (**WILL ERASE YOUR DATA**)
   * **b2c.swap**: Add a swap file in the cache device of the specified size in byte. Suffixes of k, m, g, t, p, e may be specified to denote KiB, MiB, GiB, etc... May be combined with `zram.enable=1`. Examples: `512M`, `16G`. Default: `0`
   * **b2c.volume**: Create a volume that can be mounted in the container. Example: `b2c.volume=trace-cache b2c.container="-v trace-cache:/traces docker://hello-world:latest"`. Can take the following coma-separated options:
     * **mirror**: defines where to pull from / push to. This is equivalent
       to setting separately `pull_from` and `push_to`. Use `pull_on` and
       `push_on` to control when the mirroring actions should happen.
       Syntax: `mirror=<alias>/bucket/folder/to/mirror`, with `<alias>`set up
       using `b2c.minio` option.
     * **pull_from** / **push_to**: Same syntax as `mirror`, but enables you to
       pull and push to different buckets/folders.
       Syntax: `pull_from=<alias>/bucket_from/,push_to=<alias>/bucket_to/`.
     * **pull_on** / **push_on**: Pipe-separated list of conditions that will trigger a
       mirror->volume or volume->mirror sync.
       Available conditions: `pipeline_start`, `container_start`,
       `container_end`, `pipeline_end`, and `changes`.
       The `changes` condition overrules all the others, and will make sure all
       the files have been pulled/pushed at the end of the pipeline.
       Syntax: `pull_on=pipeline_start,push_on=container_end|pipeline_end`
     * **expiration**: Specify when the volume should get deleted:
       * `never`: Keep the volume indefinitely, if possible (default);
       * `pipeline_end`: Remove the volume when the pipeline is over;
       * More options will come later.
     * **overwrite**: Overwrite object(s) if it differs from source
     * **preserve**: Preserve file(s)/object(s) attributes and bucket(s)
       policy/locking configuration(s) on target bucket(s)
     * **remove**: Remove extraneous object(s) on either side of the mirror
     * **exclude**: Exclude object(s) that match specified object name pattern
     * **encrypt_key**: Encrypt/decrypt objects on the mirror, using
       server-side encryption with customer provided keys
     * **fscrypt_key**: Encrypt locally the volume using the provided 512-bits
       base64-encoded key (`head -c 64 /dev/urandom | base64 -w 0`).
       __WARNING__: This requires a cache device and specific
       [kernel configuration](#linux-configuration).
     * **fscrypt_reset_key**: Unless this parameter is specified, the content of
       the volume will not be deleted if the provided key does not match the
       one used previously.
 * **Shutdown**:
   * **b2c.poweroff_delay**: Delay in seconds between the
     `It's now safe to turn off your computer` message, and the execution of the
     `b2c.shutdown_cmd` command. Default: `0` seconds.
   * **b2c.shutdown_cmd**: Once the execution of all the containers is over,
     execute this command. Default: `poweroff -f`

## Environment variables provided to containers

You may provide environment variables to your container by simply specifying it
in the command line of the container:

    b2c.container="-e VAR=VALUE -e VAR2=VALUE2 docker://hello-world"

Additionally, the following environment variables will be set, to help containers
decide what they want to do if a previous container failed:

 * `B2C_PIPELINE_STATUS`: `0` if all the containers' exit codes were 0, otherwise the exit code of the first container than failed. **NOTE**: `b2c.post_container`'s do not affect this variable;
 * `B2C_PIPELINE_FAILED_BY`: Command line of the first container that failed, or empty if `B2C_PIPELINE_STATUS` == 0. **NOTE**: `b2c.post_container`'s do not affect this variable;
 * `B2C_PIPELINE_PREV_CONTAINER`: Command line of the previous container in the pipeline, or empty if this is the first container;
 * `B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE`: Exit code of the previous command, or empty if this is the first container.

## How to generate an initramfs?

Generating an initramfs is pretty simple, provided you have already setup
docker/podman. Just run the following command to generate the initramfs (
located at `out/initramfs.linux_amd64.cpio`):

    $ make out/initramfs.linux_amd64.cpio
    docker run --privileged -v /home/.../boot2container/usr_mods/:/usr_mods --name boot2container boot2container
    2021/01/27 08:28:40 Disabling CGO for u-root...
    2021/01/27 08:28:40 Build environment: GOARCH=amd64 GOOS=linux GOROOT=/usr/lib/go GOPATH=/root/go CGO_ENABLED=0 GO111MODULE=off
    2021/01/27 08:28:45 Successfully built "/tmp/initramfs.linux_amd64.cpio" (size 24570272).

If you want to test your initramfs, you may test it using QEMU, provided you
have compiled a kernel according to the [Linux Configuration](#linux-configuration) section):

    $ make test KERNEL=/path/to/your/bzImage
    2021/01/27 11:07:51 Welcome to u-root!
                                _
     _   _      _ __ ___   ___ | |_
    | | | |____| '__/ _ \ / _ \| __|
    | |_| |____| | | (_) | (_) | |_
     \__,_|    |_|  \___/ \___/ \__|

    init: 2021/01/27 11:07:51 no modules found matching '/lib/modules/*.ko'
    ln: /bin/sh: File exists
    [0.97]: Busybox setup: DONE

    [1.05]: Mounting the partition /dev/vda1 to /container: DONE

    [1.05]: Container runtime setup: DONE

    [1.05]: Loading requested modules: DONE

    udhcpc: started, v1.31.1
    udhcpc: sending discover
    udhcpc: sending select for 10.0.2.15
    udhcpc: lease of 10.0.2.15 obtained, lease time 86400
    route: ioctl 0x890c failed: No such process
    [1.07]: Getting IP: DONE

    [1.07]: podman run --privileged --network=host --runtime /bin/crun-no-pivot docker://hello-world

    [    3.185548] cgroup: podman (423) created nested cgroup for controller "memory" which has incomplete hierarchy support. Nested cgroups may change behavior in the future.
    [    3.188235] cgroup: "memory" requires setting use_hierarchy to 1 on the root
    [    3.196972] kmem.limit_in_bytes is deprecated and will be removed. Please report your usecase to linux-mm@kvack.org if you depend on this functionality.

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
    3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/

    For more examples and ideas, visit:
    https://docs.docker.com/get-started/

    / #

If all goes well, you should now have a shell for you to do whatever you want.

## Linux configuration

This initramfs does not contain any module, or firmwares by default. It is thus
important to compile a kernel with everything needed built-in. Luckily, Linux
is pretty good at that!

Here are the following options you may want to set, on top of `x86_64_defconfig`,
in your `.config`:

    # For QEMU machines
    CONFIG_VIRTIO=y
    CONFIG_VIRTIO_PCI=y
    CONFIG_VIRTIO_NET=y
    CONFIG_VIRTIO_BLK=y

    # Enable CGROUPS, for podman
    CONFIG_CGROUPS=y
    CONFIG_BLK_CGROUP=y
    CONFIG_CGROUP_WRITEBACK=y
    CONFIG_CGROUP_SCHED=y
    CONFIG_CGROUP_PIDS=y
    CONFIG_CGROUP_FREEZER=y
    CONFIG_HUGETLB_PAGE=y
    CONFIG_CGROUP_HUGETLB=y
    CONFIG_CGROUP_DEVICE=y
    CONFIG_CGROUP_CPUACCT=y
    CONFIG_CGROUP_PERF=y
    CONFIG_CGROUP_DEBUG=y
    CONFIG_SOCK_CGROUP_DATA=y
    CONFIG_MEMCG=y
    CONFIG_NET=y
    CONFIG_NET_SCHED=y
    CONFIG_NET_CLS_CGROUP=y
    CONFIG_CGROUP_NET_CLASSID=y
    CONFIG_CGROUP_NET_PRIO=y

    # Enable user namespace, for podman
    CONFIG_NAMESPACES=y
    CONFIG_USER_NS=y

    # Enable OVERLAYFS, for podman
    CONFIG_OVERLAY_FS=y

    # To embed the necessary firmwares in the kernel
    CONFIG_FW_LOADER=y
    CONFIG_EXTRA_FIRMWARE_DIR=/lib/firmware"
    CONFIG_EXTRA_FIRMWARE="rtl_nic/rtl8125b-2.fw e100/d102e_ucode.bin ..."

    # For the fscrypt-based volume encryption
    CONFIG_FS_ENCRYPTION=y
    CONFIG_FS_ENCRYPTION_ALGS=y
    CONFIG_CRYPTO_CRYPTD=y
    CONFIG_CRYPTO_SIMD=y
    CONFIG_CRYPTO_CTS=y
    CONFIG_CRYPTO_ECB=y
    CONFIG_CRYPTO_XTS=y
    CONFIG_CRYPTO_SHA1=y
    CONFIG_CRYPTO_SHA1_SSSE3=y
    CONFIG_CRYPTO_SHA256_SSSE3=y
    CONFIG_CRYPTO_SHA512_SSSE3=y
    CONFIG_CRYPTO_SHA512=y
    CONFIG_CRYPTO_AES_TI=y
    CONFIG_CRYPTO_AES_NI_INTEL=y

    # For being able to run nested containers (podman in podman)
    CONFIG_VETH=y
    CONFIG_NF_CONNTRACK=y
    CONFIG_NF_NAT=y
    CONFIG_NF_NAT_MASQUERADE=y
    CONFIG_NETFILTER_XTABLES=y
    CONFIG_NETFILTER_XT_MATCH_ADDRTYPE=y
    CONFIG_NETFILTER_XT_MATCH_COMMENT=y
    CONFIG_NETFILTER_XT_MATCH_CONNTRACK=y
    CONFIG_NETFILTER_XT_MATCH_MARK=y
    CONFIG_NETFILTER_XT_MATCH_MULTIPORT=y
    CONFIG_NETFILTER_XT_NAT=y
    CONFIG_NETFILTER_XT_TARGET_MASQUERADE=y
    CONFIG_IP_NF_IPTABLES=y
    CONFIG_IP_NF_FILTER=y
    CONFIG_IP_NF_NAT=y
    CONFIG_IP_NF_TARGET_MASQUERADE=y
    CONFIG_IP6_NF_IPTABLES=y
    CONFIG_IP6_NF_FILTER=y
    CONFIG_IP6_NF_NAT=y
    CONFIG_IP6_NF_TARGET_MASQUERADE=y

    # Now, add and remove whatever you need!

    $ make oldconfig

## TODO

 * Initramfs
   * Allow downloading and mounting a tarball containing modules
   * Allow downloading/mounting a tarball containing firmwares
   * Remove the dependency in bbin (will save a couple of MB)
