#!/bin/busybox sh

suite()
{
  # Unittests
  . /app/tests/unittests/cache_device.sh
  . /app/tests/unittests/container.sh
  . /app/tests/unittests/cmdline.sh
  . /app/tests/unittests/hooks.sh
  . /app/tests/unittests/network.sh
  . /app/tests/unittests/volumes.sh

  # Integration
  . /app/tests/integration/misc.sh
  . /app/tests/integration/containers.sh
  . /app/tests/integration/volumes.sh
}

# Load shUnit2.
stdout=""
cmdline=""
. /usr/bin/shunit2
