source /app/tests/unittests/base.sh

WGET_EXIT_CODE=0
wget() {
    local OLDIFS=$IFS IFS=' '
    wget_calls="$wget_calls$@\n"
    IFS=$OLDIFS
    return $WGET_EXIT_CODE
}
wget_calls=""

testParseCmdline() {
    test() {
        # Check the defaults
        assertEquals "$ARG_CACHE_DEVICE" "none"
        assertEquals "$ARG_SWAP" ""
        assertEquals "$ARG_CONTAINER" ""
        assertEquals "$ARG_MODULES" ""
        assertEquals "$ARG_NTP_PEER" "none"
        assertEquals "$ARG_PIPEFAIL" "0"
        assertEquals "$ARG_POST_CONTAINER" ""
        assertEquals "$ARG_SHUTDOWN_CMD" "poweroff -f"
        assertEquals "$ARG_POWEROFF_DELAY" "0"
        assertEquals "$ARG_VOLUME" ""
        assertEquals "$ARG_EXTRA_ARGS_URL" ""

        # b2c.insmods
        parse_cmdline "b2c.insmods=mod1,mod2"
        assertEquals "$ARG_MODULES" "mod1,mod2"

        # b2c.cache_device
        parse_cmdline "b2c.cache_device=/dev/sda42"
        assertEquals "$ARG_CACHE_DEVICE" "/dev/sda42"

        # b2c.swap
        parse_cmdline "b2c.swap=10GB"
        assertEquals "$ARG_SWAP" "10GB"

        # b2c.container
        parse_cmdline "b2c.container=container1 b2c.container=container2"
        assertEquals "$ARG_CONTAINER" "container1\ncontainer2\n"

        # b2c.post_container
        parse_cmdline "b2c.post_container=postcontainer1 b2c.post_container=postcontainer2"
        assertEquals "$ARG_POST_CONTAINER" "postcontainer1\npostcontainer2\n"

        # b2c.poweroff_delay
        parse_cmdline "b2c.poweroff_delay=42"
        assertEquals "$ARG_POWEROFF_DELAY" "42"

        # b2c.pipefail
        parse_cmdline "b2c.pipefail"
        assertEquals "$ARG_PIPEFAIL" "1"

        # b2c.ntp_peer
        parse_cmdline "b2c.ntp_peer=ntp_peer_name"
        assertEquals "$ARG_NTP_PEER" "ntp_peer_name"

        # b2c.shutdown_cmd
        parse_cmdline "b2c.shutdown_cmd=shutmedown"
        assertEquals "$ARG_SHUTDOWN_CMD" "shutmedown"

        # b2c.shutdown_cmd
        parse_cmdline "b2c.volume=volume1 b2c.volume=volume2"
        assertEquals "$ARG_VOLUME" "volume1\nvolume2\n"

        # b2c.minio
        parse_cmdline "b2c.minio=test1 b2c.minio=test2"
        assertEquals "$ARG_MINIO" "test1\ntest2\n"

        # b2c.extra_args_url
        parse_cmdline "b2c.extra_args_url=myurl1 b2c.extra_args_url=myurl2"
        assertEquals "$ARG_EXTRA_ARGS_URL" "myurl2"
    }

    run_unit_test test
}
suite_addTest testParseCmdline


testParseExtraCmdline() {
    test() {
        WGET_EXIT_CODE=0
        wget() {
            local OLDIFS=$IFS IFS=' '
            wget_calls="$wget_calls$@\n"
            IFS=$OLDIFS
            return $WGET_EXIT_CODE
        }
        wget_calls=""

        PARSE_CMDLINE_EXIT_CODE=0
        parse_cmdline() {
            local OLDIFS=$IFS IFS=' '
            parse_cmdline_calls="$parse_cmdline_calls$@\n"
            IFS=$OLDIFS
            return $PARSE_CMDLINE_EXIT_CODE
        }
        parse_cmdline_calls=""

        start_subtest "No arguments set"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL=""
        parse_extra_cmdline
        assertEquals 0 $?
        assertEquals "" "$wget_calls"
        assertEquals "" "$parse_cmdline_calls"

        start_subtest "Normal use case"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL="https://my/url"
        echo "my b2c cmdline" > /tmp/extra_args
        parse_extra_cmdline
        assertEquals 0 $?
        assertEquals "-O /tmp/extra_args $ARG_EXTRA_ARGS_URL\n" "$wget_calls"
        assertEquals "my b2c cmdline\n" "$parse_cmdline_calls"

        start_subtest "Download fails"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL="https://my/url"
        WGET_EXIT_CODE=1
        parse_extra_cmdline
        assertEquals 1 $?
        assertEquals "-O /tmp/extra_args $ARG_EXTRA_ARGS_URL\n" "$wget_calls"
        assertEquals "" "$parse_cmdline_calls"
        WGET_EXIT_CODE=0

        start_subtest "Parsing fails"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL="https://my/url"
        PARSE_CMDLINE_EXIT_CODE=1
        parse_extra_cmdline
        assertEquals 1 $?
        assertEquals "-O /tmp/extra_args $ARG_EXTRA_ARGS_URL\n" "$wget_calls"
        assertEquals "my b2c cmdline\n" "$parse_cmdline_calls"
        PARSE_CMDLINE_EXIT_CODE=0
    }

    run_unit_test test
}
suite_addTest testParseExtraCmdline
