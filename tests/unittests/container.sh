source /app/tests/unittests/base.sh

# Tests
testRunContainers__empty_pipeline() {
    test() {
        ARG_CONTAINER=""
        ARG_POST_CONTAINER=""

        run_containers
        assertEquals 0 $?
        assertEquals 0 "$B2C_PIPELINE_STATUS"
        assertEquals "" "$B2C_PIPELINE_FAILED_BY"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE"

        assertEquals "" "$B2C_HOOK_PIPELINE_START"
        assertEquals "" "$B2C_HOOK_CONTAINER_START"
        assertEquals "" "$B2C_HOOK_CONTAINER_END"
        assertEquals "" "$B2C_HOOK_PIPELINE_END"
    }

    run_unit_test test
}
suite_addTest testRunContainers__empty_pipeline


testRunContainers__with_pipeline() {
    test() {
        ARG_CONTAINER="container1\ncontainer2"
        ARG_POST_CONTAINER="postcontainer1\npostcontainer2"

        # Mocked functions
        setup_container_runtime() {
            return 0
        }

        start_containers() {
            assertEquals "$ARG_CONTAINER" "$@"
            return 0
        }

        start_post_containers() {
            assertEquals "$ARG_POST_CONTAINER" "$@"
            return 0
        }

        container_cleanup() {
            return 0
        }

        SETUP_VOLUMES_EXIT_CODE=0
        setup_volumes() {
            return $SETUP_VOLUMES_EXIT_CODE
        }

        EXECUTE_HOOKS_EXIT_CODE=0
        EXECUTE_HOOKS_AFTER_N_INVOCATION=0
        execute_hooks() {
            EXECUTE_HOOKS_AFTER_N_INVOCATION=$((EXECUTE_HOOKS_AFTER_N_INVOCATION-1))
            if [ $EXECUTE_HOOKS_AFTER_N_INVOCATION -lt 0 ]; then
                return $EXECUTE_HOOKS_EXIT_CODE
            else
                return 0
            fi
        }

        # Test
        start_subtest "Check what happens when setup_volumes fails"
        SETUP_VOLUMES_EXIT_CODE=1
        run_containers
        assertEquals 0 $?
        assertEquals 1 "$B2C_PIPELINE_STATUS"
        assertEquals "Volume setup" "$B2C_PIPELINE_FAILED_BY"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE"

        start_subtest "Check what happens when the pipeline start hooks fail"
        SETUP_VOLUMES_EXIT_CODE=0
        EXECUTE_HOOKS_EXIT_CODE=1
        EXECUTE_HOOKS_AFTER_N_INVOCATION=0
        run_containers
        assertEquals 0 $?
        assertEquals 1 "$B2C_PIPELINE_STATUS"
        assertEquals "Pipeline-start hooks" "$B2C_PIPELINE_FAILED_BY"

        start_subtest "Check what happens when the pipeline end hooks fail after clean run"
        SETUP_VOLUMES_EXIT_CODE=0
        EXECUTE_HOOKS_EXIT_CODE=1
        EXECUTE_HOOKS_AFTER_N_INVOCATION=1
        run_containers
        assertEquals 0 $?
        assertEquals 1 "$B2C_PIPELINE_STATUS"
        assertEquals "Pipeline-end hooks" "$B2C_PIPELINE_FAILED_BY"

        start_subtest "Check what happens when the pipeline end hooks fail after failed pipeline"
        start_containers() {
            B2C_PIPELINE_STATUS=42
            B2C_PIPELINE_FAILED_BY="My test container"
            return 0
        }
        SETUP_VOLUMES_EXIT_CODE=0
        EXECUTE_HOOKS_EXIT_CODE=1
        EXECUTE_HOOKS_AFTER_N_INVOCATION=1
        run_containers
        assertEquals 0 $?
        assertEquals 42 "$B2C_PIPELINE_STATUS"
        assertEquals "My test container" "$B2C_PIPELINE_FAILED_BY"
    }

    run_unit_test test
}
suite_addTest testRunContainers__with_pipeline
