source /app/tests/unittests/base.sh


# Mocked calls
UDHCPC_EXIT_CODE=0
udhcpc() {
    local OLDIFS=$IFS IFS=' '
    udhcpc_calls="$udhcpc_calls$@\n"
    IFS=$OLDIFS
    return $UDHCPC_EXIT_CODE
}
udhcpc_calls=""


IP_EXIT_CODE=0
ip() {
    local OLDIFS=$IFS IFS=' '
    ip_calls="$ip_calls$@\n"
    IFS=$OLDIFS

    return $IP_EXIT_CODE
}
ip_calls=""

iptables_all_fail="link set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\n"

udhcpc_all_fail="-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n"

# Tests
testConnect() {
    test() {
        sleep() {
            assertEquals "0.5" "$@"
        }

        list_candidate_network_interfaces() {
            echo -e "eth0\nenp2s0f0\n"
        }

        start_subtest "Check the normal use case"
        udhcpc_calls=""
        ip_calls=""
        connect
        assertEquals 0 $?
        assertEquals "link set eth0 up\n" "$ip_calls"
        assertEquals "-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n" "$udhcpc_calls"

        start_subtest "Failed to get an IP"
        udhcpc_calls=""
        ip_calls=""
        UDHCPC_EXIT_CODE=1
        connect
        assertEquals 1 $?
        assertEquals "$iptables_all_fail" "$ip_calls"
        assertEquals "$udhcpc_all_fail" "$udhcpc_calls"
        UDHCPC_EXIT_CODE=0

        start_subtest "No cables connected"
        udhcpc_calls=""
        ip_calls=""
        IP_EXIT_CODE=1
        connect
        assertEquals 1 $?
        assertEquals "$iptables_all_fail" "$ip_calls"
        assertEquals "" "$udhcpc_calls"
        IP_EXIT_CODE=0

        start_subtest "No network interfaces available"
        list_candidate_network_interfaces() {
            /bin/true
        }
        udhcpc_calls=""
        ip_calls=""
        IP_EXIT_CODE=1
        connect
        assertEquals 1 $?
        assertEquals "" "$ip_calls"
        assertEquals "" "$udhcpc_calls"
        IP_EXIT_CODE=0
    }

    run_unit_test test
}
suite_addTest testConnect
