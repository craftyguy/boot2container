source /app/tests/unittests/base.sh

# Tests
testReset_cache_partition() {
    test() {
        FIND_CONTAINER_PARTITION_EXIT_CODE=0
        find_container_partition() {
            echo "/dev/my/partition"
            return $FIND_CONTAINER_PARTITION_EXIT_CODE
        }

        FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE=0
        find_or_create_cache_partition() {
            find_or_create_cache_partition_called=1
            return $FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE
        }
        find_or_create_cache_partition_called=0

        FORMAT_CACHE_PARTITION_EXIT_CODE=0
        format_cache_partition() {
            format_cache_partition_called=1
            return $FORMAT_CACHE_PARTITION_EXIT_CODE
        }
        format_cache_partition_called=0

        start_subtest "Resetting when no previous container was used"
        FIND_CONTAINER_PARTITION_EXIT_CODE=1
        FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE=42
        reset_cache_partition
        assertEquals 42 $?
        assertEquals 0 $format_cache_partition_called
        FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE=0
        FIND_CONTAINER_PARTITION_EXIT_CODE=0

        start_subtest "Formatting failed"
        FORMAT_CACHE_PARTITION_EXIT_CODE=1
        reset_cache_partition
        assertEquals 1 $?
        assertEquals 1 $find_or_create_cache_partition_called
        FORMAT_CACHE_PARTITION_EXIT_CODE=0

        start_subtest "Everything worked"
        reset_cache_partition
        assertEquals 0 $?
    }

    run_unit_test test
}
suite_addTest testReset_cache_partition


testMount_cache_partition() {
    test() {
        MOUNT_EXIT_CODE=0
        mount() {
            assertEquals "$CONTAINER_PART_DEV" "$1"
            assertEquals "$CONTAINER_MOUNTPOINT" "$2"
            mount_called=1
            return $MOUNT_EXIT_CODE
        }
        mount_called=0

        MKDIR_EXIT_CODE=0
        mkdir() {
            assertEquals "$CONTAINER_MOUNTPOINT" "$@"
            return $MKDIR_EXIT_CODE
        }

        FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE=0
        find_or_create_cache_partition() {
            find_or_create_cache_partition_called=1
            return $FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE
        }
        find_or_create_cache_partition_called=0

        RESET_CACHE_PARTITION_EXIT_CODE=0
        reset_cache_partition() {
            reset_cache_partition_called=1
            return $RESET_CACHE_PARTITION_EXIT_CODE
        }
        reset_cache_partition_called=0

        CONTAINER_MOUNTPOINT=/tmp/test

        start_subtest "No cache partition wanted"
        ARG_CACHE_DEVICE="none"
        mount_cache_partition
        assertEquals 0 $?

        start_subtest "Auto-select a drive, but no drives are present"
        ARG_CACHE_DEVICE="auto"
        FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE=1
        find_or_create_cache_partition_called=0
        mount_called=0
        mount_cache_partition
        assertEquals 0 $?
        assertEquals 1 $find_or_create_cache_partition_called
        assertEquals 0 $mount_called
        FIND_OR_CREATE_CACHE_PARTITION_EXIT_CODE=0

        start_subtest "Auto-select a drive"
        ARG_CACHE_DEVICE="auto"
        mount_called=0
        find_or_create_cache_partition_called=0
        mount_cache_partition
        assertEquals 0 $?
        assertEquals 1 $find_or_create_cache_partition_called
        assertEquals 1 $mount_called

        start_subtest "Reset a drive, but no drives are present"
        ARG_CACHE_DEVICE="reset"
        RESET_CACHE_PARTITION_EXIT_CODE=1
        reset_cache_partition_called=0
        mount_called=0
        mount_cache_partition
        assertEquals 0 $?
        assertEquals 1 $reset_cache_partition_called
        assertEquals 0 $mount_called
        RESET_CACHE_PARTITION_EXIT_CODE=0

        start_subtest "Reset a drive"
        ARG_CACHE_DEVICE="reset"
        mount_called=0
        reset_cache_partition_called=0
        mount_cache_partition
        assertEquals 0 $?
        assertEquals 1 $reset_cache_partition_called
        assertEquals 1 $mount_called
    }

    run_unit_test test
}
suite_addTest testMount_cache_partition
