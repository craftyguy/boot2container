source /app/tests/unittests/base.sh


# Tests
testExecuteHook() {
    test() {
        start_subtest "Check that hooks can execute multiple commands"
        execute_hooks "touch /tmp/file1\ntouch /tmp/file2\n"
        assertEquals 0 $?
        assertTrue 'The first file was not created' "[ -f /tmp/file1 ]"
        assertTrue 'The second file was not created' "[ -f /tmp/file2 ]"

        start_subtest "Check what happens upon failure"
        execute_hooks "touch /missing/folder"
        assertEquals 1 $?
    }

    run_unit_test test
}
suite_addTest testExecuteHook
