source /app/tests/integration/base.sh

testVolumes() {
    qemu_params="$QEMU_NIC $QEMU_DISK"

    reset_disk

    # Check the persistence of data in a volume between containers, and then that mirroring to a folder works
    kernel_cmdline='b2c.volume=myvol,mirror=/tmp/volume,push_on=container_end,fscrypt_key=LvOC5xFSrMxJymf2XOwHGPBj7x1mENBuBWxV1a10LBgV5LG29VTgCpjlR4Ng7j0e5vTPH2WlAcU0R/mPYlPttw== \
        b2c.cache_device=auto \
        b2c.container="-v myvol:/myvol docker.io/library/busybox:latest touch /myvol/result" \
        b2c.container="-v myvol:/myvol docker.io/library/busybox:latest [ -f /myvol/result ]" \
        b2c.container="-v /tmp/volume:/mnt_volume docker.io/library/busybox:latest [ -f /mnt_volume/result ]"'

    run_test

    assertContains "$stdout" "Execution is over, pipeline status: 0"
}
suite_addTest testVolumes
