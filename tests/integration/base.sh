QEMU_NO_NIC="-nic none"
QEMU_NIC="-nic user,model=virtio-net-pci"
QEMU_DISK="-drive file=/disk.img,format=raw,if=virtio"
DISK_PATH=/disk.img

reset_disk() {
    rm "$DISK_PATH" 2> /dev/null
    fallocate -l 128M "$DISK_PATH"
}

run_test() {
    tmpfile=$(mktemp /tmp/qemu_exec.XXXXXX)

    cmdline="qemu-system-x86_64 -kernel /kernel -initrd /initramfs.linux_amd64.cpio \
-nographic -m 768M -enable-kvm $qemu_params \
-append 'console=ttyS0 $kernel_cmdline'"
    eval "timeout 30 $cmdline" | tee "$tmpfile"

    stdout=$(cat "$tmpfile")
    rm "$tmpfile"
}
