source /app/tests/integration/base.sh

testDefaultContainerPipeline() {
    qemu_params="$QEMU_NIC $QEMU_DISK"
    kernel_cmdline='b2c.container="-e EXIT_CODE=42 docker://stakater/exit-container" \
        b2c.container="-e EXIT_CODE=43 docker://stakater/exit-container" \
        b2c.post_container="-e EXIT_CODE=44 docker://stakater/exit-container" \
        b2c.post_container="-e EXIT_CODE=45 docker://stakater/exit-container"'
    reset_disk
    run_test

    # First container
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=0'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE='
    assertContains "$stdout" "Exiting with code: 42"
    assertContains "$stdout" "The container exited with error code 42, continuing..."

    # Second container, executed despite the previous one failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=42'
    assertContains "$stdout" "Exiting with code: 43"
    assertContains "$stdout" "The container exited with error code 43, continuing..."

    # First post-container, executed despite the pipeline failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=43 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=43'
    assertContains "$stdout" "Exiting with code: 44"

    # Second post-container, executed despite the previous post container failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=44 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=44'
    assertContains "$stdout" "Exiting with code: 45"

    # Check the overall pipeline status
    assertContains "$stdout" "Execution is over, pipeline status: 42"
}
suite_addTest testDefaultContainerPipeline

testContainerPipelineWithPipeFail() {
    qemu_params="$QEMU_NIC $QEMU_DISK"
    kernel_cmdline='b2c.container="docker://stakater/exit-container" \
        b2c.container="-e EXIT_CODE=42 docker://stakater/exit-container" \
        b2c.container="-e EXIT_CODE=43 docker://stakater/exit-container" \
        b2c.post_container="-e EXIT_CODE=44 docker://stakater/exit-container" \
        b2c.post_container="-e EXIT_CODE=45 docker://stakater/exit-container" \
        b2c.pipefail'
    reset_disk
    run_test

    # Successful container, so we can run the next one
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=0'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE='
    assertContains "$stdout" "Exiting with code: 0"
    assertContains "$stdout" "The container run successfully, load the next one!"

    # First failing container
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE='
    assertContains "$stdout" "Exiting with code: 42"
    assertContains "$stdout" "The container exited with error code 42, aborting the pipeline..."

    # Second failing container (should not have been executed)
    assertNotContains "$stdout" "Exiting with code: 43"

    # First post-container, executed right after the first failing container
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=42'
    assertContains "$stdout" "Exiting with code: 44"

    # Second post-container, executed despite the previous post container failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=44 docker://stakater/exit-container"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=44'
    assertContains "$stdout" "Exiting with code: 45"

    # Check the overall pipeline status
    assertContains "$stdout" "Execution is over, pipeline status: 42"
}
suite_addTest testContainerPipelineWithPipeFail
