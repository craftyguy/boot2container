#!/bin/busybox sh

# Copyright (c) 2021 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Peres <martin.peres@mupuf.org>
#

MODULES_PATH=/usr_mods
CONTAINER_MOUNTPOINT=/container
CONTAINER_ROOTFS="$CONTAINER_MOUNTPOINT/rootfs"
CONTAINER_CACHE="$CONTAINER_MOUNTPOINT/cache"
CONTAINER_CACHE_SWAPFILE="$CONTAINER_MOUNTPOINT/swapfile"
CACHE_PARTITION_LABEL="B2C_CACHE"

function log {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    echo -e "\n[$(busybox cut -d ' ' -f1 /proc/uptime)]: $*\n"
    set "-$prev_shell_config"
}

function setup_busybox {
    for cmd in `busybox --list`; do
        [ -f "/bbin/$cmd" ] || [ -f "/bin/$cmd" ] || busybox ln -s /bin/busybox /bin/$cmd
    done
    log "Busybox setup: DONE"
}

function setup_mounts {
    mount -t proc none /proc
    mount -t sysfs none /sys
    mount -t devtmpfs none /dev
    mkdir -p /dev/pts
    mount -t devpts devpts /dev/pts

    # Mount cgroups
    mount -t cgroup2 none /sys/fs/cgroup
    cd /sys/fs/cgroup/
    for sys in $(awk '!/^#/ { if ($4 == 1) print $1 }' /proc/cgroups); do
        mkdir -p $sys
        if ! mount -n -t cgroup -o $sys cgroup $sys; then
            rmdir $sys || true
        fi
    done

    log "Mounts setup: DONE"
}

function setup_env {
    export HOME=/root
    export PATH=/bbin:$PATH
}

# Execute a list of newline-separated list of commands
function execute_hooks {
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for hook_cmd in $(echo -e "$1"); do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        $hook_cmd || { log "The command failed with error code $?"; return 1; }

        { set +x; } 2> /dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS
    set "-$prev_shell_config"
}

ARG_CACHE_DEVICE="none"
ARG_SWAP=""
ARG_CONTAINER=""
ARG_MODULES=""
ARG_NTP_PEER="none"
ARG_PIPEFAIL="0"
ARG_POST_CONTAINER=""
ARG_SHUTDOWN_CMD="poweroff -f"
ARG_POWEROFF_DELAY="0"
ARG_VOLUME=""
ARG_MINIO=""
ARG_EXTRA_ARGS_URL=""

function parse_cmdline {
    local cmdline=$1

    # Go through the list of options in the commandline, but remove the quotes
    # around multi-words arguments. Awk seems to be doing that best.
    local OLDIFS=$IFS IFS=$'\n'
    for param in $(echo "$cmdline" | awk -F\" 'BEGIN { OFS = "" } {
        for (i = 1; i <= NF; i += 2) {
            gsub(/[ \t]+/, "\n", $i)
        }
        print
    }'); do
        IFS=$OLDIFS

        value="${param#*=}"
        case $param in
            b2c.insmods=*)
                ARG_MODULES=$value
                ;;
            b2c.cache_device=*)
                ARG_CACHE_DEVICE=$value
                ;;
            b2c.swap=*)
                ARG_SWAP=$value
                ;;
            b2c.container=*)
                ARG_CONTAINER="$ARG_CONTAINER$value\n"
                ;;
            b2c.post_container=*)
                ARG_POST_CONTAINER="$ARG_POST_CONTAINER$value\n"
                ;;
            b2c.poweroff_delay=*)
                ARG_POWEROFF_DELAY="$value"
                ;;
            b2c.pipefail)
                ARG_PIPEFAIL="1"
                ;;
            b2c.ntp_peer=*)
                ARG_NTP_PEER=$value
                ;;
            b2c.shutdown_cmd=*)
                ARG_SHUTDOWN_CMD="$value"
                ;;
            b2c.volume=*)
                ARG_VOLUME="$ARG_VOLUME$value\n"
                ;;
            b2c.minio=*)
                ARG_MINIO="$ARG_MINIO$value\n"
                ;;
            b2c.extra_args_url=*)
                ARG_EXTRA_ARGS_URL="$value"
                ;;
        esac

        IFS=$'\n'
    done
    IFS=$OLDIFS
    # TODO: add a parameter to download a volume with firmwares and modules
}

function load_modules {
    [ -z "$@" ] && return

    for mod_name in `echo "$@" | busybox tr ',' '\n'`; do
        path="$MODULES_PATH/$mod_name"
        echo "Load the module: $path"
        insmod "$path"
    done

    log "Loading requested modules: DONE"
}

function list_candidate_network_interfaces {
    find /sys/class/net/ -type l -exec basename {} \; | grep -E '^(eth|en)' || return 0
}

function connect {
    [ -z "$(list_candidate_network_interfaces)" ] && {
        echo "No suitable network interfaces found"
        return 1
    }

    for i in $(seq 0 0.5 10)
    do
        for ifname in $(list_candidate_network_interfaces)
        do
            if ip link set $ifname up; then
                udhcpc -i $ifname -s /etc/uhdcp-default.sh -T 1 -n || continue

                log "Getting IP: DONE"
                return 0
            else
                sleep 0.5 2> /dev/null
            fi
        done
    done

    return 1
}

function ntp_set {
    case $1 in
        none)
            log "WARNING: Did not reset the time, use b2c.ntp_peer=auto to set it on boot"
            return 0
            ;;
        auto)
            peer_addr="pool.ntp.org"
            ;;
        *)
            peer_addr=$1
    esac

    # Limit the maximum execution time to prevent the boot sequence to be stuck
    # for too long
    status="DONE"
    time timeout 5 ntpd -dnq -p "$peer_addr" || status="FAILED"

    log "Getting the time from the NTP server $peer_addr: $status"
}

function parse_extra_cmdline {
    # Exit early if we do not have any extra
    [ -z "$ARG_EXTRA_ARGS_URL" ] && return 0

    log "Parse the extra command line"
    wget -O /tmp/extra_args "$ARG_EXTRA_ARGS_URL" || {
        log "ERROR: Could not download the extra command line, shutting down!"
        return 1
    }

    log "Parse the extra command line"
    parse_cmdline "$(cat /tmp/extra_args)" || {
        log "ERROR: Failed to parse the extra command line, shutting down!"
        return 1
    }

    return 0
}

function find_container_partition {
    dev_name=`blkid | grep "LABEL=\"$CACHE_PARTITION_LABEL\"" | head -n 1 | cut -d ':' -f 1`
    if [ -n "$dev_name" ]; then
        echo $dev_name
        return 0
    else
        return 1
    fi
}

function format_cache_partition {
    log "Formating the partition $CONTAINER_PART_DEV"

    # Enable unconditionally the encryption
    mkfs.ext4 -O encrypt -F -L "$CACHE_PARTITION_LABEL" "$CONTAINER_PART_DEV"
}

function format_disk {
    if [ -n "$1" ]; then
        parted --script $1 mklabel gpt
        parted --script $1 mkpart primary ext4 2048s 100%

        CONTAINER_PART_DEV=`lsblk -no PATH $1 | tail -n -1`
        format_cache_partition

        return $?
    fi

    return 1
}

function find_or_create_cache_partition {
    # See if we have an existing block device that would work
    CONTAINER_PART_DEV=`find_container_partition` && return 0

    # Find a suitable disk
    sr_disks_majors=`grep ' sr' /proc/devices | sed "s/^[ \t]*//" | cut -d ' ' -f 1 | tr '\n' ',' | sed 's/,$//'`
    disk=`lsblk -ndlfp -e "$sr_disks_majors" | head -n 1`

    if [ -n "$disk" ]; then
        log "No existing cache partition found on this machine, create one from the disk $disk"

        # Find a disk, partition it, then format it as ext4
        format_disk $disk || return 1

        return 0
    else
        log "No disks founds, continue without a cache"

        return 1
    fi
}

function reset_cache_partition {
    log "Reset the cache partition"

    # Find the cache partition, and if missing, default to
    # the same behaviour as "auto".
    CONTAINER_PART_DEV=`find_container_partition` || {
        find_or_create_cache_partition
        return $?
    }

    # Found the partition, reformat it!
    format_cache_partition || return 1

    return 0
}

function try_to_use_cache_device {
    # Check if the parameter is a path to a file
    if [ -f "$ARG_CACHE_DEVICE" ]; then
        log "The caching parameter '$ARG_CACHE_DEVICE' is neither 'none', 'auto', or a path to a block device. Defaulting to 'none'"
        return 0
    fi

    # $ARG_CACHE_DEVICE has to be a path to a drive
    # NOTE: Pay attention to the space after $ARG_CACHE_DEVICE, as it
    # makes sure that we don't accidentally match /dev/sda1 when asking
    # for /dev/sda.
    blk_dev=`lsblk -rpno PATH,TYPE,LABEL | grep "$ARG_CACHE_DEVICE "`
    if [ -z "$blk_dev" ]; then
        log "Error: The device '$ARG_CACHE_DEVICE' is neither a block device, nor a partition. Defaulting to no caching."
        return 1
    fi

    path=$(echo "$blk_dev" | cut -d ' ' -f 1)
    type=$(echo "$blk_dev" | cut -d ' ' -f 2)
    label=$(echo "$blk_dev" | cut -d ' ' -f 3)
    case $type in
        part)
            CONTAINER_PART_DEV="$path"
            if [ -z "$label" ]; then
                format_cache_partition
                return $?
            fi
            ;;

        disk)
            # Look for the first partition from the drive $1, that has the right cache
            CONTAINER_PART_DEV=`lsblk -no PATH,LABEL $path | grep "$CACHE_PARTITION_LABEL" | cut -d ' ' -f 1 | head -n 1`
            if [ -n "$CONTAINER_PART_DEV" ]; then
                return 0
            else
                log "No existing cache partition on the drive $path, recreate the partition table and format a partition"
                format_disk $path
                return $?
            fi
            ;;
    esac

    return 0
}

function mount_swap_file {
    [ -f "$CONTAINER_CACHE_SWAPFILE" ] && rm "$CONTAINER_CACHE_SWAPFILE"

    fallocate -l "$ARG_SWAP" "$CONTAINER_CACHE_SWAPFILE" || return 1
    mkswap "$CONTAINER_CACHE_SWAPFILE"  || return 1
    swapon "$CONTAINER_CACHE_SWAPFILE"  || return 1

    return 0
}

function unmount_swap_file {
    # Remove the swap file, if present
    if [ -f "$CONTAINER_CACHE_SWAPFILE" ]; then
        swapoff $CONTAINER_CACHE_SWAPFILE
        rm $CONTAINER_CACHE_SWAPFILE
    fi
}

CONTAINER_PART_DEV=""
function mount_cache_partition {
    [ -d "$CONTAINER_MOUNTPOINT" ] || mkdir "$CONTAINER_MOUNTPOINT"

    # Find a suitable cache partition
    case $ARG_CACHE_DEVICE in
        none)
            log "Do not use a partition cache"
            return 0
            ;;
        auto)
            find_or_create_cache_partition || return 0
            ;;
        reset)
            reset_cache_partition || return 0
            ;;
        *)
            try_to_use_cache_device "$ARG_CACHE_DEVICE" || return 0
            ;;
    esac

    log "Selected the partition $CONTAINER_PART_DEV as a cache"

    status="DONE"
    mount "$CONTAINER_PART_DEV" "$CONTAINER_MOUNTPOINT" || status="FAILED"
    log "Mounting the partition $CONTAINER_PART_DEV to $CONTAINER_MOUNTPOINT: $status"

    # If the partition has been mounted
    if [ $status == 'DONE' ]; then
        log "Checking the available space in the cache partition"

        # Check how much disk space is available in it
        df -h $CONTAINER_PART_DEV

        # Mount the swap file, if asked
        if [ -n "$ARG_SWAP" ]; then
            log "Mounting a swap file"

            status="DONE"
            mount_swap_file || status="FAILED"

            log "Mounting a swap file of $ARG_SWAP: $status"
        fi
    fi

    return 0
}

function unmount_cache_partition {
    [ -z "$CONTAINER_PART_DEV" ] && return

    sync

    status="DONE"
    unmount_swap_file || status="FAILED"
    log "Unmounting the swap file: $status"

    status="DONE"
    umount $CONTAINER_PART_DEV || status="FAILED"
    log "Remounting the partition $CONTAINER_PART_DEV read-only: $status"
}

function setup_container_runtime {
    # HACK: I could not find a way to change the right parameter in podman's
    # config, so make a symlink for now
    [ -d "$CONTAINER_CACHE" ] || mkdir "$CONTAINER_CACHE"
    [ -f "/var/tmp" ] || ln -s "$CONTAINER_CACHE" /var/tmp

    # Squash a kernel warning
    echo 1 > /sys/fs/cgroup/memory/memory.use_hierarchy

    # Set some configuration files
    touch /etc/hosts
    echo "root:x:0:0:root:/root:/bin/sh" > /etc/passwd
    echo "containers:165536:65537" > /etc/subuid
    echo "containers:165536:65537" > /etc/subgid

    log "Container runtime setup: DONE"
}

function start_background_cmd {
    $@ &> /dev/null &

    # Hide the rest of the execution
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local pid=$!

    # Give the shell some time to write the previous command
    sleep 0.01

    # Make sure to kill the service at the end of the pipeline
    B2C_HOOK_PIPELINE_END="${B2C_HOOK_PIPELINE_END}kill -9 $pid\n"
    set "-$prev_shell_config"
}

function queue_pipeline_end_cmd {
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    B2C_HOOK_PIPELINE_END="${B2C_HOOK_PIPELINE_END}$@\n"
    set "-$prev_shell_config"
}

function call_mcli_mirror_on_hook_conditions {
    local conditions=$1
    local mcli_args=$2

    local has_non_changes_cond=0
    local has_changes_cond=0

    local cmd="mcli mirror $mcli_args"
    local watch_cmd="mcli mirror --watch $mcli_args"

    # If "changes" is set, we need to handle that and ignore the rest
    local OLDIFS=$IFS IFS='|'
    for condition in ${conditions}; do
        IFS=$OLDIFS
        case "${condition}" in
            pipeline_start|container_start|container_end|pipeline_end)
                has_non_changes_cond=1
                ;;
            changes)
                # Start the --watch command in the background
                B2C_HOOK_PIPELINE_START="${B2C_HOOK_PIPELINE_START}start_background_cmd $watch_cmd\n"

                # Since we want to make sure that all the changes made by the container have been pushed
                # back to minio before shutting down the machine, and since we can't signal to mcli's
                # mirror operation we want to exit as soon as all the currently-pending transfers are over,
                # we first need to kill the 'mcli mirror --watch' process before starting the final sync.
                # The start_background_cmd function already set up the killing of the background process
                # at pipeline_end, so all we have to do is queue the final mirroring command. To do so,
                # just add a pipeline_start hook that will run right after the start_background_cmd,
                # and will add the command to the pipeline_end hook list. Sorry for the mess!
                B2C_HOOK_PIPELINE_START="${B2C_HOOK_PIPELINE_START}queue_pipeline_end_cmd $cmd\n"

                has_changes_cond=1
                ;;
            *)
                echo "B2C_WARNING: The hook condition '$condition' is unknown"
                ;;
        esac
        IFS='|'
    done

    if [ "$has_changes_cond" -eq 0 ]; then
        local OLDIFS=$IFS IFS='|'
        for condition in ${conditions}; do
            IFS=$OLDIFS
            case "${condition}" in
                pipeline_start)
                    B2C_HOOK_PIPELINE_START="${B2C_HOOK_PIPELINE_START}${cmd}\n"
                    ;;
                container_start)
                    B2C_HOOK_CONTAINER_START="${B2C_HOOK_CONTAINER_START}${cmd}\n"
                    ;;
                container_end)
                    B2C_HOOK_CONTAINER_END="${B2C_HOOK_CONTAINER_END}${cmd}\n"
                    ;;
                pipeline_end)
                    B2C_HOOK_PIPELINE_END="${B2C_HOOK_PIPELINE_END}${cmd}\n"
                    ;;
            esac
            IFS='|'
        done
    elif [ "$has_non_changes_cond" -eq 1 ]; then
        # Friendly reminder to distracted users
        echo "B2C_WARNING: When the 'changes' condition is set, all other conditions are ignored"
    fi
}

function setup_volume {
    # Disable logging for volume parsing
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    local volume_name=${@%%,*}
    local pull_url=''
    local push_url=''
    local pull_on=''
    local push_on=''
    local expiration='never'
    local mirror_extra_args=''
    local fscrypt_key=''
    local fscrypt_reset_key="0"

    log "Set up the $volume_name volume"

    # Parse the coma-separated list of arguments.
    # NOTICE: We add a coma at the end of the command line so as to be able to
    # remove the volume name in case there would be no parameters
    volume_params="$@,"
    local OLDIFS=$IFS IFS=,
    for spec in ${volume_params#*,}; do
        IFS=$OLDIFS

        case $spec in
            mirror=*)
                pull_url="${spec#mirror=}"
                push_url="${spec#mirror=}"
                ;;
            pull_from=*)
                pull_url="${spec#pull_from=}"
                ;;
            push_to=*)
                push_url="${spec#push_to=}"
                ;;
            pull_on=*)
                pull_on="${spec#pull_on=}"
                ;;
            push_on=*)
                push_on="${spec#push_on=}"
                ;;
            expiration=*)
                expiration="${spec#expiration=}"
                ;;
            overwrite)
                mirror_extra_args="$mirror_extra_args --overwrite"
                ;;
            remove)
                mirror_extra_args="$mirror_extra_args --remove"
                ;;
            exclude=*)
                mirror_extra_args="$mirror_extra_args --exclude ${spec#exclude=}"
                ;;
            encrypt_key=*)
                mirror_extra_args="$mirror_extra_args --encrypt-key ${spec#encrypt_key=}"
                ;;
            preserve)
                mirror_extra_args="$mirror_extra_args -a"
                ;;
            fscrypt_key=*)
                fscrypt_key="${spec#fscrypt_key=}"
                ;;
            fscrypt_reset_key)
                fscrypt_reset_key="1"
                ;;
            *)
                echo "B2C_WARNING: The parameter $spec is unknown"
                ;;
        esac
        IFS=,
    done
    IFS=$OLDIFS

    # Parse the expiration policy
    # TODO: Allow setting expiration dates with a format compatible with `date "+1 day"
    if [ -n "$expiration" ]; then
        case $expiration in
            never|pipeline_end)
                # Nothing to do, as these are valid
                ;;
            *)
                log "ERROR: Unknown value for expiration: $expiration"
                return 1
                ;;
        esac
    fi

    # Parsing is over, re-enable logging
    set "-$prev_shell_config"

    # Create the volume, if it does not exist
    if ! podman volume exists "$volume_name" ; then
        podman volume create --label "expiration=$expiration" "$volume_name" || return 1
    fi

    # Get the volume's mount point, and make sure the volume exists
    local local_dir=$(podman volume inspect --format "{{.Mountpoint}}" $volume_name)
    [ -d "$local_dir" ] || {
        mkdir -p "$local_dir" || return 1
    }

    # Check if the volume was already encrypted
    fscryptctl get_policy "$local_dir" &> /dev/null && volume_already_encrypted=1 || volume_already_encrypted=0

    # Enable the encryption, if wanted
    if [ -n "$fscrypt_key" ]; then
        local key_id=""

        echo "Enabling encryption:"
        echo "  - Current status: volume_encrypted=$volume_already_encrypted"
        echo "  - Adding the fscrypt key to the volume mount point"
        key_id=$(echo "$fscrypt_key" | base64 -d | fscryptctl add_key "$local_dir") || {
            # If this fails, this means the kernel/filesystem does not support encryption, or the
            # partition has not been created with "-O encrypt".
            echo "ERROR: You may need to reset the cache using 'b2c.cache_device=reset' to enable encryption if it was created with boot2container <= 0.9"
            return 1
        }

        echo "  - Setting the policy on the volume mount point"
        if ! fscryptctl set_policy "$key_id" "$local_dir"; then
            # Setting the policy failed. This can be due to a kernel with missing config, a
            # non-empty folder, or simply trying to use the wrong key.

            # If the directory is not encrypted, or if we asked to reset the key
            if [ "$volume_already_encrypted" -eq "0" ] || [ "$fscrypt_reset_key" -eq "1" ]; then
                echo "Re-try applying the policy after resetting the volume"

                # Remove the volume then re-create it
                rm -rf "$local_dir" || return 1
                mkdir "$local_dir" || return 1

                # Try setting the policy again
                fscryptctl set_policy "$key_id" "$local_dir" || return 1
            elif [ "$volume_already_encrypted" -eq "1" ]; then
                echo "ERROR: Missing kernel config options, or wrong fscrypt key provided"
                return 1
            fi
        fi

        # Make sure the volume is encrypted
        fscryptctl get_policy "$local_dir" || return 1
    elif [ "$volume_already_encrypted" -eq 1 ]; then
        log "ERROR: Trying to use an already-encrypted volume without providing a key"
        return 1
    fi

    # Mirror setup
    if [ -n "$pull_url" ] && [ -n "$push_url" ] ; then
        # Set up the mirroring operations
        local mcli_mirror_args="-r $local_dir -q --no-color $mirror_extra_args"
        call_mcli_mirror_on_hook_conditions "$pull_on" "$mcli_mirror_args $pull_url ."
        call_mcli_mirror_on_hook_conditions "$push_on" "$mcli_mirror_args . $push_url"
    fi
}

function remove_expired_volumes {
    volumes=$(podman volume list --noheading --format {{.Name}} --filter label=expiration=pipeline_end) || return $?

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for volume_name in $volumes; do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        podman volume rm --force "$volume_name"

        { set +x; } 2>/dev/null
        IFS='\n'
    done
    IFS=$OLDIFS
    set "-$prev_shell_config"
}

function setup_volumes {
    # Remove all the expired volumes, before setting up the new ones
    log "Remove expired volumes"
    remove_expired_volumes

    # Set all the minio aliases before we potentially try using them
    log "Set up the minio aliases"
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for minio in $(echo -e "$ARG_MINIO"); do
        IFS=$OLDIFS
        minio_cmd=$(echo $minio | tr ',' ' ')
        set "-$prev_shell_config"

        mcli --no-color alias set $minio_cmd || return 1

        { set +x; } 2>/dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS

    log "Create the volumes"
    local OLDIFS=$IFS IFS=$'\n'
    for volume in $(echo -e "$ARG_VOLUME"); do
        IFS=$OLDIFS

        set "-$prev_shell_config"
        setup_volume $volume || {
            set "-$prev_shell_config"
            return 1
        }
        { set +x; } 2>/dev/null

        IFS=$'\n'
    done
    IFS=$OLDIFS

    log "Setting up volumes: DONE"

    # Now that the early boot is over, let's log every command executed
    set "-$prev_shell_config"
}

function create_container {
    # Podman is not super good at explaining what went wrong when creating a
    # container, so just try multiple times, each time increasing the size of
    # the hammer!
    for i in 0 1 2 3 4; do
        { cmdline="podman create --rm --privileged --pull=always --network=host \
--runtime /bin/crun-no-pivot -e B2C_PIPELINE_STATUS=$B2C_PIPELINE_STATUS \
-e B2C_PIPELINE_FAILED_BY=\"$B2C_PIPELINE_FAILED_BY\" \
-e B2C_PIPELINE_PREV_CONTAINER=\"$B2C_PIPELINE_PREV_CONTAINER\" \
-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE"; } 2> /dev/null

        # Set up the wanted container
        container_id=`eval "$cmdline $@"` && podman init "$container_id" && return 0

        # The command failed... Ignore the first 3 times, as we want to check it
        # is not a shortlived-network error
        if [ $i -eq 3 ]; then
            # Try resetting the entire state of podman, before trying again!
            podman system reset -f
        fi

        sleep 1
    done

    return 1
}

function start_container {
    log "Execute the container_start hooks"
    execute_hooks "$B2C_HOOK_CONTAINER_START" || return 1

    log "Pull, create, and init the container"
    { container_id=""; } 2> /dev/null
    create_container $@ || return 1

    # Make sure that the layers and volumes got pushed to the drive before
    # running the container
    sync

    # HACK: Figure out how to use "podman wait" to wait for the container to be
    # ready for execution. Without this sleep, we sometimes fail to attach the
    # stdout/err to the container. Even a one ms sleep is sufficient in my
    # testing, but let's add a bit more just to be sure
    sleep .1

    log "About to start executing a container"
    exit_code=0
    podman start -a "$container_id" || exit_code=$?

    # Store the results of the execution
    B2C_PIPELINE_PREV_CONTAINER="$@"
    B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE="$exit_code"

    log "Execute the container_end hooks"
    execute_hooks "$B2C_HOOK_CONTAINER_END" || return 1

    return $exit_code
}

function start_containers {
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        IFS=$OLDIFS

        exit_code=0
        set "-$prev_shell_config"
        start_container "$container_params" || exit_code=$?

        { set +x; } 2> /dev/null
        if [ $exit_code -eq 0 ] ; then
            log "The container run successfully, load the next one!"
        else
            # If this is the first container that failed, store that information
            if [ $B2C_PIPELINE_STATUS -eq 0 ]; then
                B2C_PIPELINE_STATUS="$exit_code"
                B2C_PIPELINE_FAILED_BY="$container_params"
            fi

            if [ $ARG_PIPEFAIL -eq 1 ]; then
                log "The container exited with error code $exit_code, aborting the pipeline..."
                set "-$prev_shell_config"
                { return 0; } 2> /dev/null
            else
                log "The container exited with error code $exit_code, continuing..."
            fi
        fi

        IFS=$'\n'
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"
    { return 0; } 2> /dev/null
}

function start_post_containers {
    log "Running the post containers"

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        start_container "$container_params" || /bin/true

        { set +x; } 2>/dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"
    { return 0; } 2> /dev/null
}

function container_cleanup {
    # Stop and delete all the containers that may still be running.
    # This should be a noop, but I would rather be safe than sorry :)

    # There is a race in podman https://github.com/containers/podman/issues/4314
    # Copy a similar dance done in the upstream CI for podman push
    podman container stop -a || { sleep 2; podman container stop -a; }

    podman umount -a -f
    podman container rm -fa

    # Remove all the dangling images
    podman image prune -f
}

function run_containers {
    # Hide all the default values
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    B2C_PIPELINE_STATUS="0"
    B2C_PIPELINE_FAILED_BY=""
    B2C_PIPELINE_PREV_CONTAINER=""
    B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=""
    B2C_HOOK_PIPELINE_START=""
    B2C_HOOK_CONTAINER_START=""
    B2C_HOOK_CONTAINER_END=""
    B2C_HOOK_PIPELINE_END=""
    set "-$prev_shell_config"

    if [ -n "$ARG_CONTAINER$ARG_POST_CONTAINER" ]; then
        setup_container_runtime  # TODO: Add tests for this
        if setup_volumes; then
            log "Execute the pipeline_start hooks"
            if execute_hooks "$B2C_HOOK_PIPELINE_START"; then
                log "Start the containers pipeline"

                start_containers "$ARG_CONTAINER"
                start_post_containers "$ARG_POST_CONTAINER"

                log "Done executing the pipeline, clean up time!"
                container_cleanup

                log "Execute the pipeline_end hooks"
                execute_hooks "$B2C_HOOK_PIPELINE_END" || {
                    if [ "$B2C_PIPELINE_STATUS" -eq 0 ]; then
                        B2C_PIPELINE_STATUS=1
                        B2C_PIPELINE_FAILED_BY="Pipeline-end hooks"
                    fi
                }
            else
                B2C_PIPELINE_STATUS=1
                B2C_PIPELINE_FAILED_BY="Pipeline-start hooks"
            fi
        else
            B2C_PIPELINE_STATUS=1
            B2C_PIPELINE_FAILED_BY="Volume setup"
        fi

        log "Execution is over, pipeline status: ${B2C_PIPELINE_STATUS}"

        # We are done with the execution, remove all volumes that expired
        log "Remove expired volumes"
        remove_expired_volumes
    fi
}

function print_runtime_info {
    log "## Runtime information ##\n
Linux version: $(cat /proc/sys/kernel/osrelease)\n\
Boot2container version: $(cat /etc/b2c.version)\n\
Architecture: $(uname -m)\n\
CPU: $(cat /proc/cpuinfo | grep "model name" | head -n 1 | cut -d ':' -f 2- | cut -d ' ' -f 2-)\n\
RAM: $(cat /proc/meminfo | grep "MemTotal" | head -n 1 | rev | cut -d ' ' -f 1-2 | rev)\n\
Block devices: $(lsblk -rdno NAME,SIZE | tr ' ' '=' | tr '\n' ' ')"
}

function main {
    set -eu

    # Initial setup
    setup_busybox
    #setup_mounts  # To be continued to so we could boot without any go commands
    setup_env

    # Initial information about the machine
    print_runtime_info

    # Parse the kernel command line, in search of the b2c parameters
    parse_cmdline "$(busybox cat /proc/cmdline)"

    # Now that the early boot is over, let's log every command executed
    set -x

    # Load the user-requested modules
    log "Load the kernel modules wanted by the user"
    load_modules $ARG_MODULES

    # Mount the cache partition
    log "Mount the cache partition"
    mount_cache_partition

    # Connect to the network, now that the modules are loaded
    log "Connect to the network"
    if connect; then
        # Set the time
        log "Synchronize the clock"
        ntp_set $ARG_NTP_PEER

        # Download the extra arguments
        if parse_extra_cmdline; then
            # Start the containers
            log "Run the containers"
            run_containers
        fi
    else
        log "ERROR: Could not connect to the network, shutting down!"
    fi

    # Prepare for the shutdown
    log "Unmount the cache partition"
    unmount_cache_partition

    # Shutdown the machine
    log "It's now safe to turn off your computer"  # I feel old...
    sleep $ARG_POWEROFF_DELAY || /bin/true
    eval $ARG_SHUTDOWN_CMD
}

# Call the main function, unless the entire file is being unit-tested
[ "${UNITTEST:-0}" -eq 0 ] && main
