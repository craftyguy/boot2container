# Copyright (c) 2021 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Peres <martin.peres@mupuf.org>
#

SHELL := /bin/bash

DOCKER ?= docker

IMAGE_LABEL ?= registry.freedesktop.org/mupuf/boot2container
TEST_IMAGE_LABEL = registry.freedesktop.org/mupuf/boot2container/test

CONTAINER_LABEL ?= boot2container

# TODO: Collect all wanted modules and load them
out/initramfs.linux_amd64.cpio:
	@mkdir out usr_mods 2> /dev/null || /bin/true
	@rm out/initramfs.linux_amd64.cpio 2> /dev/null || /bin/true
	git describe --dirty --always --tags > out/b2c.version
	@-$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null 2> /dev/null || /bin/true
	$(DOCKER) create --name $(CONTAINER_LABEL) $(IMAGE_LABEL)
	$(DOCKER) cp usr_mods $(CONTAINER_LABEL):/usr_mods
	$(DOCKER) cp config/containers $(CONTAINER_LABEL):/etc/containers/
	$(DOCKER) cp config/cni $(CONTAINER_LABEL):/etc/cni/
	$(DOCKER) cp uhdcp-default.sh $(CONTAINER_LABEL):/etc/uhdcp-default.sh
	$(DOCKER) cp crun-no-pivot $(CONTAINER_LABEL):/bin/
	$(DOCKER) cp initscript.sh $(CONTAINER_LABEL):/bin/
	$(DOCKER) cp out/b2c.version $(CONTAINER_LABEL):/etc/
	$(DOCKER) start -a $(CONTAINER_LABEL)
	@$(DOCKER) cp $(CONTAINER_LABEL):/tmp/initramfs.linux_amd64.cpio out/ > /dev/null
	@$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null

out/initramfs.linux_amd64.cpio.xz: out/initramfs.linux_amd64.cpio
	xz --check=crc32 -9 --lzma2=dict=1MiB --stdout out/initramfs.linux_amd64.cpio | dd conv=sync bs=512 of=out/initramfs.linux_amd64.cpio.xz

rebuild_containers:
	$(DOCKER) build -t $(IMAGE_LABEL) .
	$(DOCKER) build -t $(TEST_IMAGE_LABEL) -f tests/Dockerfile .

out/disk.img:
	fallocate -l 128M out/disk.img

test: out/initramfs.linux_amd64.cpio out/disk.img
	[ -f "$(KERNEL)" ] || (echo "ERROR: Set the KERNEL parameter, pointing to linux kernel with modules compiled in"; exit 1)
	$(DOCKER) run --rm --name b2c_test --device=/dev/kvm -v $(PWD)/out/initramfs.linux_amd64.cpio:/initramfs.linux_amd64.cpio -v $(KERNEL):/kernel -v $(PWD):/app $(TEST_IMAGE_LABEL) /app/tests/tests.sh

manual_test: out/initramfs.linux_amd64.cpio out/disk.img
	[ -f "$(KERNEL)" ] || (echo "ERROR: Set the KERNEL parameter, pointing to linux kernel with modules compiled in"; exit 1)
	qemu-system-x86_64 -drive file=out/disk.img,format=raw,if=virtio -nic user,model=virtio-net-pci -kernel $(KERNEL) -initrd out/initramfs.linux_amd64.cpio -nographic -m 384M -enable-kvm -append 'console=ttyS0 b2c.container=docker://docker.io/library/hello-world b2c.container="-ti docker://docker.io/library/alpine:latest" b2c.cache_device=auto b2c.shutdown_cmd="sh"'

clean:
	-rm out/initramfs.linux_amd64.cpio
	-rm out/initramfs.linux_amd64.cpio.xz
	-rm out/b2c.version
	-$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null
